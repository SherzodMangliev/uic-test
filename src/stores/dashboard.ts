import { defineStore } from 'pinia'
import { getDashboard } from '@/services/dashboard.service'

export const useDashboard = defineStore('dashboard', {
    state: () => {
        return {
            stats: {
                total_paid: 9890000,
                total_need: 1696478662,
                total_must_pay: 1686588662
            }
        }
    },
    actions: {
        fetchDashboard() {
            return new Promise((resolve, reject) => {
                getDashboard().then(res => {
                    this.stats = res.data
                    resolve(res.data)
                }).catch(err => {
                    reject(err)
                })
            })
        }
    }
});