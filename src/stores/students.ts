import { defineStore } from 'pinia'
import { getStudentsList } from '@/services/students.service'

export const useStudents = defineStore('students', {
    state: () => {
        return {
            students: [],
            students_count: 0
        }
    },
    actions: {
        fetchStudents(params?: any) {
            return new Promise((resolve, reject) => {
                getStudentsList(params).then(res => {
                    this.students = res.data.results
                    this.students_count = res.data.count
                    resolve(res.data)
                }).catch(err => {
                    reject(err)
                })
            })
        }
    },
    getters: {
        getStudents (state) {
            return state.students
        }
    }
});