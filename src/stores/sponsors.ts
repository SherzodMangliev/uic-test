import { defineStore } from 'pinia'
import { getSponsorDetails, getSponsorsList, updateSponsorDetails } from '@/services/sponsors.service'

export const useSponsors = defineStore('sponsors', {
    state: () => {
        return {
            sponsors: [],
            sponsors_count: 0,
            single_sponsor: {}
        }
    },
    actions: {
        fetchSponsors(params?: any) {
            return new Promise((resolve, reject) => {
                getSponsorsList(params).then(res => {
                    this.sponsors = res.data.results
                    this.sponsors_count = res.data.count
                    resolve(res.data)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        fetchSponsorDetails(id: number) {
            return new Promise((resolve, reject) => {
                getSponsorDetails(id).then((res) => {
                    this.single_sponsor = res.data
                    resolve(res.data)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        editSponsor (data: any) {
            return new Promise((resolve, reject) => {
                updateSponsorDetails(data).then((res) => {
                    this.single_sponsor = res.data
                    resolve(res.data)
                }).catch(err => {
                    reject(err)
                })
            })
        }
    },
    getters: {
        getSponsors (state) {
            return state.sponsors
        }
    }
});