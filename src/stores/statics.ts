import { defineStore } from 'pinia'
import { getPaymentTypeList, getTariffList } from '@/services/statics.service'

export const useStatics = defineStore('statics', {
    state: () => {
        return {
            tariffs: [],
            payment_types: []
        }
    },
    actions: {
        fetchPaymentTypes() {
            return new Promise((resolve, reject) => {
                getPaymentTypeList().then(res => {
                    this.payment_types = res.data
                    resolve(res.data)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        fetchTariffs() {
            return new Promise((resolve, reject) => {
                getTariffList().then(res => {
                    this.tariffs = res.data
                    resolve(res.data)
                }).catch(err => {
                    reject(err)
                })
            })
        }
    },
});