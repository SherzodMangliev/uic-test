import api from './api'

export function getTariffList () {
    return api.get('/tariff-list/')
}

export function getPaymentTypeList () {
    return api.get('/payment-type-list/')
}