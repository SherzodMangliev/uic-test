import axios from 'axios'
import TokenService from './token.service'

const instance = axios.create({
    baseURL: import.meta.env.VITE_BACKEND_PATH,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
    },
})

instance.interceptors.request.use(
    config => {
        const token = TokenService.getToken()

        if (token && config.url !== '/auth/login/') {
            config.headers['Authorization'] = 'Bearer ' + token          // config.headers['x-access-token'] = token // for Node.js Express back-end
        }
        return config
    },
    error => {
        return Promise.reject(error)
    },
)

instance.interceptors.response.use(
    res => {
        return res
    },
    async err => {
        const originalConfig = err.config
        if (originalConfig.url !== '/auth/login/' && err.response) {
            // if (err.response.status === 401 && !originalConfig._retry) {
            //     originalConfig._retry = true;
            //     try {
            //       const rs = await instance.post("/auth/refreshtoken", {
            //         refreshToken: TokenService.getLocalRefreshToken(),
            //       });
            //       const { accessToken } = rs.data;
            //       TokenService.updateLocalAccessToken(accessToken);
            //       return instance(originalConfig);
            //     } catch (_error) {
            //       return Promise.reject(_error);
            //     }
            //   }
            if (err.response.status === 401) {
                try {
                    TokenService.removeUser()

                    window.location.assign('/login')
                    return instance(originalConfig)
                } catch (_error) {
                    return Promise.reject(_error)
                }
            }

        }

        return Promise.reject(err)
    },
)

// Vue.prototype.$http = instance

export default instance
