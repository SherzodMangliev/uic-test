import api from './api'

export function getSponsorsList(params?: any) {
    console.log(params)
    return api.get('/sponsor-list/', { params })
}

export function getSponsorDetails(id: number) {
    return api.get(`/sponsor-detail/${id}/`)
}

export function updateSponsorDetails(data: any) {
    return api.patch(`/sponsor-update/${data.id}/`, data)
}