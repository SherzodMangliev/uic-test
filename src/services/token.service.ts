import type { LoginResp } from './auth.service'

const TokenService = {

    getToken() {
      const access_token = JSON.parse(localStorage.getItem('access_token'))
      return access_token
    },

    // getRefreshToken() {

    // }
  
    getUser() {
      return JSON.parse(localStorage.getItem('user_data'))
    },

    setUser(user) {
      localStorage.setItem('user_data', JSON.stringify(user))
    },
  
    setTokens(tokens: LoginResp) {
      localStorage.setItem('access_token', JSON.stringify(tokens.access))
      localStorage.setItem('refresh_token', JSON.stringify(tokens.refresh))
    },
  
    removeTokensAndUser() {
      localStorage.removeItem('access_token')
      localStorage.removeItem('refresh_token')
      localStorage.removeItem('user_data')
    }
  }
  
  export default TokenService
  