import api from './api'
import TokenService from './token.service'
import jwt_decode from 'jwt-decode'

export interface LoginBody {
    username: string
    password: string
}

export interface LoginResp {
    access: string
    refresh: string
}

export function login(data: LoginBody) : Promise<LoginResp> {
    return new Promise((resolve, reject) => {
        api.post('/auth/login/', data).then((res) => {
            TokenService.setTokens(res.data)
            const user_data = jwt_decode(res.data.access)
            TokenService.setUser(user_data)
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}