import api from './api'

export function getStudentsList (params: any) {
    return api.get('/student-list/', { params })
}