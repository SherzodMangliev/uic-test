export function useMoneyFormat (money: number) {
    if (money) {
        return new Intl.NumberFormat('ru-Ru', { style: 'currency', currency: 'UZS' }).format(money)
    } else {
        return money
    }
}