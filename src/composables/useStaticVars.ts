export const status_options = [
    {
      value: '1',
      label: 'Barchasi'
    },
    {
      value: '2',
      label: 'Yangi'
    },
    {
      value: '3',
      label: 'Moderatsiyada'
    },
    {
      value: '4',
      label: 'Tasdiqlangan'
    },
    {
      value: '5',
      label: 'Bekor qilingan'
    }
  ]
  
export const sposnorshim_summ_options = [
    {
      label: '1000000',
      value: 1000000
    },
    {
      label: '5000000',
      value: 5000000
    },
    {
      label: '7000000',
      value: 7000000
    },
    {
      label: '10000000',
      value: 10000000
    },
    {
      label: '30000000',
      value: 30000000
    },
    {
      label: '50000000',
      value: 50000000
    }
  ]