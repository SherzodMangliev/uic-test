export function getFormattedDate (date: string) {
    return new Date(date).toLocaleString().split(',')[0]
}