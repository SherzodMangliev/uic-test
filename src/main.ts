import { createApp } from 'vue'
import './tailwind.css'
import 'vue-select/dist/vue-select.css'
import App from './App.vue'
import { createPinia } from 'pinia'
import { createRouter, createWebHistory } from 'vue-router/auto'
import { createHead } from '@vueuse/head'
import Maska from 'maska'
import TokenService from './services/token.service'

const app = createApp(App)
const head = createHead()
const pinia = createPinia()
app.use(Maska)
app.directive('maska', Maska.maska)

const router = createRouter({
  history: createWebHistory(),
})


router.beforeEach((to, from, next) => {
  console.log(to)
  if (to.name !== '/login') {
    const isLoggedIn = TokenService.getToken()
    if (isLoggedIn) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})
app.use(pinia)
app.use(router)
app.use(head)
app.mount(document.body)
